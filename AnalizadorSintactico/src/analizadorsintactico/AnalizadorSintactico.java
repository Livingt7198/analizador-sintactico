/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadorsintactico;

import analizadorsintactico.interfaz.infzAnalizadorSintactico;
import analizadorsintactico.metodosGramatica.Metodos;
import static analizadorsintactico.metodosGramatica.Metodos.Posiciones_Iniciales;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author D&S2
 */
public class AnalizadorSintactico {
    private static infzAnalizadorSintactico analizadorSintactico;
    public AnalizadorSintactico() {
        
    }
    public static String path ;
    

       private static Metodos metodos ;
       private static String[] auxa;
       
        public static String cargarArchivo(){
        JFileChooser Abrir = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Archivo Fuente", "txt");
        Abrir.setFileFilter(filter);
        int returnVal = Abrir.showOpenDialog(Abrir);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: "
                    + Abrir.getSelectedFile().getName());
        }
        path=Abrir.getName();
        return Abrir.getSelectedFile().getPath();
        
       
    }  
     public static String nombre()
     {
         return path;
     }   

    public static void main(String[] args) throws IOException {
        
//       String []v2=metodos.LecturaArchivo(cargarArchivo());
//    /*    for (int i = 0; i < v2.length; i++) {
//            if(v2[i]!=null)
//            System.out.println(v2[i]);
//        }
//        */
//       ArrayList a =metodos.Primer_Split(v2);
//        Posiciones_Iniciales(a);
//      String[] aux = metodos.getPosicion2inicia();
//        System.out.println(aux[1]);
//        
//        ArrayList b = metodos.Split2();
//        
//        Metodos.VEcCaracteres();
//        Metodos.busquedaTokens();
//        
//       // Metodos.Eliminar_Igual();
//        Metodos.ambiguedad();
//        Metodos.Recursion();
//        Metodos.Gramaticas_Sin_AB();
//        Metodos.Gramaticas_Libres_Contexto();
//        String []v1=metodos.LecturaArchivo2();
       String[] v3 = GenerarGramaticaNueva(cargarArchivo());
     ArrayList a1= Metodos.Primer_Split_Libre(v3);
       
        Metodos.Posiciones_Iniciales_Libres(a1);
        ArrayList c = Metodos.Split2_Libres();
        Metodos.VEcCaracteres_Gra_Libre();
        Metodos.Conjuntos_Primero(0);
        Metodos.Rectificar();
        Metodos.ConjuntosSiguientes();
        Metodos.Mostra();
        Metodos.Conjunto_S();
    }
        
    public static String[] GenerarGramaticaNueva(String path) throws IOException{
        
        String []v2=metodos.LecturaArchivo(path);
    /*    for (int i = 0; i < v2.length; i++) {
            if(v2[i]!=null)
            System.out.println(v2[i]);
        }
        */
       ArrayList a =metodos.Primer_Split(v2);
        Posiciones_Iniciales(a);
      String[] aux = metodos.getPosicion2inicia();
        System.out.println(aux[1]);
        
        ArrayList b = metodos.Split2();
        
        Metodos.VEcCaracteres();
        Metodos.busquedaTokens();
        
       // Metodos.Eliminar_Igual();
        Metodos.ambiguedad();
        Metodos.Recursion();
        Metodos.Gramaticas_Sin_AB();
        Metodos.Gramaticas_Libres_Contexto();
        String []v1=metodos.LecturaArchivo2();
        return v1;
    }
    
    
    public static String getPath() {
        return path;
    }

    public static void setPath(String path) {
        AnalizadorSintactico.path = path;
    }
    
    
}
