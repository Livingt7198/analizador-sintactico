
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analizadorsintactico.metodosGramatica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.*;

/**
 *
 * @author D&S2
 */
public class Metodos {

    private static String path, AuxPath;
    private static File fileEntrada, fileSalidaG;
    private static FileReader reader;
    private static FileWriter writer;
    private static BufferedReader BReader;
    private static BufferedWriter BWriter;
    private static int j = 0, w = 0, k = 0, Indice, h = 0;
    private static String P, B, PIn, P_pri, PinA, RecursionIzq, Sig, siguiente;
    private static String RecursionPrima, Ambiguedad1, Ultimo, aux4, pp;
    private static ArrayList<String[]> Lista_Principal = new ArrayList<>();
    private static ArrayList<String[]> Lista_Principal_Libre = new ArrayList<>();
    private static String[] Linea, AuxP;
    private static String[] posicion_InicialIZQ;
    private static String[] posicion_InicialIZQ_Libre;
    private static String[] posicion_InicialDER;
    private static String[] posicion_InicialDER_Libre;
    private static ArrayList<String[]> Lista_PrincipalDere = new ArrayList<>();
    private static ArrayList<String[]> Lista_PrincipalDere_Libre = new ArrayList<>();
    private static ArrayList<String> Lista_Principal_LetrasDere = new ArrayList<String>();
    private static ArrayList<char[]> LetrasAux = new ArrayList<>();
    private static ArrayList<String[]> LetrasAux_Gra_Libres = new ArrayList<>();
    private static ArrayList<String[]> Lista_Conjuntos_Primeros = new ArrayList<>();
    private static ArrayList<String> Lista_Conjuntos_Primeros_Inicial = new ArrayList<>();
    private static ArrayList<String> Lista_Conjuntos_Primeros_Final = new ArrayList<>();
    private static ArrayList<String> Conjuntos_Primeros = new ArrayList<>();
    private static char[] AuxLetras;
    private static ArrayList<String> ListaTokens;
    private static ArrayList<String> Lista_Principal_Gramatica = new ArrayList<String>();
    private static ArrayList<String> Conjuntos_Siguiente = new ArrayList<>();

    public static String[] LecturaArchivo(String Path) throws FileNotFoundException, IOException {
        String linea = "";
        path = Path;
        fileEntrada = new File(Path);
        reader = new FileReader(fileEntrada);
        BReader = new BufferedReader(reader);

        String Vec[] = new String[1000];

        System.out.println();
        while ((linea = BReader.readLine()) != null) {
            Vec[j] = linea;
            //System.out.println(Vec[j]);
            j++;
        }

        return Vec;
    }

    public static ArrayList Primer_Split(String vec[]) {

        String SplitAux[] = null;
        for (int i = 0; i < j; i++) {
            SplitAux = null;
            SplitAux = vec[i].split(":");
            Lista_Principal.add(SplitAux);

        }

        return Lista_Principal;
    }

    public static void Posiciones_Iniciales(ArrayList<String[]> a) {
        posicion_InicialIZQ = new String[j];
        posicion_InicialDER = new String[j];
        a = Lista_Principal;
        String m[] = null;

        for (int i = 0; i < j; i++) {
            m = null;
            m = a.get(i);
            posicion_InicialIZQ[i] = m[0];
            posicion_InicialDER[i] = m[1];
        }
    }

    public static ArrayList Split2() {

        String SplitAux[] = null;
        for (int i = 0; i < j; i++) {
            SplitAux = null;
            SplitAux = posicion_InicialDER[i].split("/");
            Lista_PrincipalDere.add(SplitAux);
        }
        return Lista_PrincipalDere;
    }

    public static ArrayList<char[]> VEcCaracteres() {
        int aa = 0;
        int b = 0;
        for (int i = 0; i < Lista_PrincipalDere.size(); i++) {
            String[] ao = (String[]) Lista_PrincipalDere.get(i);

            for (w = 0; w < ao.length; w++) {
                LetrasAux.add(ao[w].toCharArray());

            }
        }
        return LetrasAux;
    }

    public static void busquedaTokens() {
        AuxLetras = new char[10];
        ListaTokens = new ArrayList();
        for (int i = 0; i < j; i++) {
            for (int l = 0; l < LetrasAux.size(); l++) {
                AuxLetras = LetrasAux.get(l);
                for (int m = 0; m < AuxLetras.length; m++) {
                    if (posicion_InicialIZQ[i].toString().equals(Character.toString(AuxLetras[m]))) {

                        //       System.out.println("No Es un toquen" + posicion_InicialIZQ[i] + "y" + AuxLetras[m]);
                    } else {
                        String s = Character.toString(AuxLetras[m]);
                        //System.err.println("Es un token" + posicion_InicialIZQ[i] + "y" + AuxLetras[m]);

                        ListaTokens.add(Character.toString(AuxLetras[m]));

                    }

                }

            }
        }

        realizarTokensstatic();
    }

    public static ArrayList<String> realizarTokensstatic() {
        int e = j;
        for (int l = 0; l < e; l++) {
            if (ListaTokens.size() >= (LetrasAux.size() - j)) {
                for (int i = 0; i < ListaTokens.size(); i++) {
                    for (int s = 0; s < ListaTokens.size(); s++) {

                        if (ListaTokens.get(i).equals(ListaTokens.get(s)) && i != s) {
                            ListaTokens.remove(i);
                        } else {
                            if (ListaTokens.get(i).equals(posicion_InicialIZQ[l].charAt(0))) {
                                ListaTokens.remove(i);
                            }
                        }
                    }

                }
            }

        }

        Tokenizar_ParteIzq();
        Asignar_Id_token();
        return ListaTokens;

    }

    public static void Tokenizar_ParteIzq() {
        for (int i = ListaTokens.size() - 1; i >= 0; i--) {
            String aux = ListaTokens.get(i);
            for (int l = 0; l < posicion_InicialIZQ.length; l++) {
                String aux2 = posicion_InicialIZQ[l];
                if (aux2.equals(aux) && Character.isUpperCase(aux.charAt(0))) {
                    ListaTokens.remove(i);
                }
            }
        }
    }

    public static void Asignar_Id_token() {
        for (int i = 0; i < j; i++) {
            if (posicion_InicialDER[i].contains("id") == true) {
                for (int l = 0; l < ListaTokens.size(); l++) {
                    if (ListaTokens.get(l).equals("d")) {
                        ListaTokens.set(l, "id");
                    }
                }
            }
        }
        ListaTokens.add("#");
    }

    public static void Recursion() {
        int a = 0;
        for (int i = 0; i < LetrasAux.size(); i++) {
            //System.out.println("Vector" + (i + 1));
            char[] AuxLetras = LetrasAux.get(i);

            for (int m = 0; m < j; m++) {
                a = 0;
                if (true) {

                }
                if (posicion_InicialIZQ[m].charAt(0) == AuxLetras[0] && AuxLetras.length > 1) {
                    String[] ao = Lista_PrincipalDere.get(m);
                    for (int l = 0; l < 1; l++) {
                        if (ao[l].contains(String.valueOf(AuxLetras[l + 1])) == true) {
                            P_pri = String.valueOf(AuxLetras[ao.length] + "" + AuxLetras[0] + '´');
                            RecursionIzq = posicion_InicialIZQ[m] + ":" + P_pri;
                            RecursionPrima = String.valueOf(AuxLetras[0] + "´" + ":" + AuxLetras[m] + AuxLetras[ao.length] + AuxLetras[0] + "´" + "/" + "#");
                            Lista_Principal_Gramatica.add(RecursionIzq);
                            Lista_Principal_Gramatica.add(RecursionPrima);
                            //System.out.println(RecursionIzq);
                            //System.out.println(RecursionPrima);
                            Indice++;
                        }

                    }
                }

            }
        }
    }

    public static void ambiguedad() {
        boolean a = true;
        boolean aa = true;
        String[] aux1 = new String[j];
        for (int i = 0; i < Lista_PrincipalDere.size(); i++) {
            String[] ao = (String[]) Lista_PrincipalDere.get(i);

            int m = 0;
            while (m < (ao.length - 1)) {

                for (m = 1; m < ao.length; m++) {
                    for (int l = (ao.length - 1); l >= 0; l--) {

                        if ((ao[m].contains(ao[l])) == true && a == true) {
                            // System.out.println("entra" + ao[m] + "  " + ao[l]);
                            aux1[0] = ao[l] + posicion_InicialIZQ[i] + '´';
                            B = ao[m].replace(ao[l], "");
//                            System.out.println(aux1[i]);
                            PinA = posicion_InicialIZQ[i] + ":" + aux1[i] + "/";
                            Ambiguedad1 = String.valueOf(posicion_InicialIZQ[i] + '´' + ":" + B);
                            Lista_Principal_Gramatica.add(Ambiguedad1);
                            a = false;
                            Indice++;
                        } else {
                            if (((ao[m - 1].contains(ao[l])) == false) && aa == true) {
                                P = ao[m - 1];
//                                System.out.println("esto es P   " + P);
                                PIn = PinA + P;
//                                System.out.println(PIn);
                                Lista_Principal_Gramatica.add(PIn);

                                aa = false;
                            }
                        }

                    }

                }
            }

        }

    }

    public static ArrayList Gramaticas_Sin_AB() {
        for (int i = Indice; i < j; i++) {
            Ultimo = posicion_InicialIZQ[i];
            Ultimo = Ultimo + ":" + posicion_InicialDER[i];
            Lista_Principal_Gramatica.add(Ultimo);
        }
        Lista_Principal_Gramatica.set(0, PIn);
        Lista_Principal_Gramatica.set(1, Ambiguedad1);
        return Lista_Principal_Gramatica;
    }

    public static void Gramaticas_Libres_Contexto() throws IOException {
//        fileSalidaG = new File(path)
        //System.out.println( fileEntrada.getName());
        AuxPath = path.replace(fileEntrada.getName(), "Libres_de_Contexto.txt");

        fileSalidaG = new File(AuxPath);
        writer = new FileWriter(fileSalidaG);
        BWriter = new BufferedWriter(writer);
        if (!fileSalidaG.exists() == true) {
            fileSalidaG.createNewFile();
        }

        for (int i = 0; i < Lista_Principal_Gramatica.size(); i++) {
            System.out.println(Lista_Principal_Gramatica.get(3));
            if (i > 0) {
                BWriter.write("\n");
            }
            BWriter.write(Lista_Principal_Gramatica.get(i).toString());

        }

        BWriter.close();

    }

    public static void GenerarTxtLibre_Contextos(String path, ArrayList Lista) throws IOException {
//        fileSalidaG = new File(path)
        //System.out.println( fileEntrada.getName());
        AuxPath = path.replace(fileEntrada.getName(), "Libres_de_Contexto.txt");

        fileSalidaG = new File(AuxPath);
        writer = new FileWriter(fileSalidaG);
        BWriter = new BufferedWriter(writer);
        if (!fileSalidaG.exists() == true) {
            fileSalidaG.createNewFile();
        }

        for (int i = 0; i < Lista.size(); i++) {
            System.out.println(Lista.get(3));
            if (i > 0) {
                BWriter.write("\n");
            }
            BWriter.write(Lista.get(i).toString());

        }

        BWriter.close();

    }

    public static String[] LecturaArchivo2() throws FileNotFoundException, IOException {
        String linea = "";

        fileEntrada = new File(AuxPath);
        reader = new FileReader(fileEntrada);
        BReader = new BufferedReader(reader);

        String Vec[] = new String[1000];

        //System.out.println();
        while ((linea = BReader.readLine()) != null) {
            Vec[h] = linea;
            //System.out.println(Vec[j]);
            h++;
        }

        return Vec;
    }

    public static ArrayList Primer_Split_Libre(String vec[]) {

        String SplitAux[] = null;
//         System.out.println(h);

        for (int i = 0; i < h; i++) {
            SplitAux = null;
            SplitAux = vec[i].split(":");
            Lista_Principal_Libre.add(SplitAux);
        }

        return Lista_Principal_Libre;
    }

    public static void Posiciones_Iniciales_Libres(ArrayList<String[]> a) {
        posicion_InicialIZQ_Libre = new String[h];
        posicion_InicialDER_Libre = new String[h];
        a = Lista_Principal_Libre;
        String m[] = null;

        for (int i = 0; i < h; i++) {
            m = null;
            m = a.get(i);
            posicion_InicialIZQ_Libre[i] = m[0];
            posicion_InicialDER_Libre[i] = m[1];
        }
    }

    public static ArrayList Split2_Libres() {

        String SplitAux[] = null;
        for (int i = 0; i < h; i++) {
            SplitAux = null;
            SplitAux = posicion_InicialDER_Libre[i].split("/");
            Lista_PrincipalDere_Libre.add(SplitAux);
        }
        return Lista_PrincipalDere_Libre;
    }

    public static ArrayList<String[]> VEcCaracteres_Gra_Libre() {
        int aa = 0;
        int b = 0;
        for (int i = 0; i < Lista_PrincipalDere_Libre.size(); i++) {
            String[] ao = (String[]) Lista_PrincipalDere_Libre.get(i);

            for (w = 0; w < ao.length; w++) {
                LetrasAux_Gra_Libres.add(ao[w].split("|"));

            }
        }
        concatenar_comillasSencillas();
        return LetrasAux_Gra_Libres;
    }

    public static void concatenar_comillasSencillas() {
        for (int i = 0; i < LetrasAux_Gra_Libres.size(); i++) {
            String[] auxChar = LetrasAux_Gra_Libres.get(i);
            for (int l = 0; l < auxChar.length; l++) {
                if (auxChar[l].equals("´")) {
                    String concAux = auxChar[l - 1] + auxChar[l];
                    auxChar[l] = null;

                    auxChar[l - 1] = concAux;
                    LetrasAux_Gra_Libres.set(i, auxChar);
                }
            }
        }
    }

    public static void Conjuntos_Primero(int q) {

        for (int i = q; i < h; i++) {
            int c = 0;
            String[] VecAux = Lista_PrincipalDere_Libre.get(i);
            for (int l = 0; l < VecAux.length; l++) {
                AuxP = new String[VecAux.length];
                String aux2 = String.valueOf(VecAux[l].charAt(0));
                for (int m = 0; m < ListaTokens.size(); m++) {
                    String aux3 = ListaTokens.get(m);
                    if (aux3.equals(aux2) && VecAux[l] != null) {

                        AuxP[l] = aux2;
                        System.out.println("Vec " + l + " " + AuxP[l]);

                        if (aux2.equals("i") && String.valueOf(VecAux[l].charAt(1)).equals("d")) {
                            pp = aux2 + String.valueOf(VecAux[l].charAt(1));

                        }
                    } else {
                        for (int n = 0; n < posicion_InicialIZQ_Libre.length; n++) {
                            if (aux2.equals(posicion_InicialIZQ_Libre[n])) {
                                AuxP[l] = posicion_InicialIZQ_Libre[n];
                            }
                        }

                    }
                }
                Lista_Conjuntos_Primeros_Inicial.add(AuxP[l]);
                c++;

            }
            if (Lista_Conjuntos_Primeros_Inicial.get(0).equals(Lista_Conjuntos_Primeros_Inicial.get(c - 1))) {
                aux4 = Lista_Conjuntos_Primeros_Inicial.get(0);
                Lista_Conjuntos_Primeros_Inicial.removeAll(Lista_Conjuntos_Primeros_Inicial);

            } else {
                if ((Lista_Conjuntos_Primeros_Inicial.get(0) + Lista_Conjuntos_Primeros_Inicial.get(c - 1)).equals("(i")) {
                    Lista_Conjuntos_Primeros_Inicial.set(c - 1, pp);

                }
                //System.err.println(Lista_Conjuntos_Primeros_Inicial.get(0) + Lista_Conjuntos_Primeros_Inicial.get(c - 1));
                aux4 = Lista_Conjuntos_Primeros_Inicial.get(0) + "," + Lista_Conjuntos_Primeros_Inicial.get(c - 1);

                Lista_Conjuntos_Primeros_Inicial.removeAll(Lista_Conjuntos_Primeros_Inicial);
            }
            Lista_Conjuntos_Primeros_Final.add(aux4);
        }
    }

    public static ArrayList Rectificar() {
        int m = 1;
        for (int i = 0; i < Lista_Conjuntos_Primeros_Final.size(); i++) {
            for (int l = 0; l < posicion_InicialIZQ_Libre.length; l++) {

                if (Lista_Conjuntos_Primeros_Final.get(i).equals(posicion_InicialIZQ_Libre[l])) {
                    String aux = Lista_Conjuntos_Primeros_Final.get(i);
                    Lista_Conjuntos_Primeros_Final.set(i, "Prim(" + aux + ")");
                }
            }
            Conjuntos_Primeros.add("Prim(" + posicion_InicialIZQ_Libre[i] + ")={" + Lista_Conjuntos_Primeros_Final.get(i) + "}");
        }
        return Conjuntos_Primeros;
    }

    public static ArrayList ConjuntosSiguientes() {
//        for (int i = 0; i < h; i++)
//        {
        String ConcaAux = "";
        for (int l = 0; l < LetrasAux_Gra_Libres.size(); l++) {
            String[] aux = LetrasAux_Gra_Libres.get(l);

            for (int m = 0; m < aux.length; m++) {
                String letra_aux = aux[m];

                for (int i = 0; i < posicion_InicialIZQ_Libre.length; i++) {

                    String Letra_Izq = posicion_InicialIZQ_Libre[i];
                    if (Letra_Izq.equals(letra_aux) && m != aux.length - 1) {
                        //System.out.println(letra_aux + "   Es iagual a " + Letra_Izq);
                        for (int n = 0; n < ListaTokens.size(); n++) {
                            String tokenAux = ListaTokens.get(n);
                            if ((tokenAux.equals(aux[m + 1])) == true && letra_aux != null) {
                                //System.out.println(aux[m + 1]);
                                ConcaAux = ConcaAux + " " + aux[m + 1];

                                siguiente = "Sig(" + letra_aux + ")={" + ConcaAux + "";
                                //     System.out.println(siguiente);
                                Conjuntos_Siguiente.add(siguiente);
                            }
                        }
                        for (int n = 0; n < posicion_InicialIZQ_Libre.length; n++) {
                            String valid = posicion_InicialIZQ_Libre[n];
                            if (aux[m + 1] != null && aux[m + 1].equals(posicion_InicialIZQ_Libre[n])) {
                                //System.out.println(Lista_Conjuntos_Primeros_Final.get(n).charAt(0));
                                String siguiente = "Sig(" + letra_aux + ")={" + Lista_Conjuntos_Primeros_Final.get(n).charAt(0);
                                Conjuntos_Siguiente.add(siguiente);
                            }

                        }
                        if (aux[m + 1] == null) {
                            Sig = aux[m];
                            aux[m + 1] = Sig;
                            //System.out.println("La siguiente de"+aux[m]+" es" + posicion_InicialIZQ_Libre[l]);
                            if (Sig.equals(aux[m + 1])) {
                                String sigi = "Sig(" + aux[m] + ")={" + posicion_InicialIZQ_Libre[i - 1] + "}";
                                //System.out.println("ES SIGI"+sigi);
                                if (aux[m + 1].equals(aux[m])) {
                                    Conjuntos_Siguiente.add(sigi);
                                }

                            }
                        }
                    }
                }

            }

        }
//         

        String aux1 = Conjuntos_Siguiente.get(1);
        Conjuntos_Siguiente.set(1, aux1 + " " + "$" + "}");
        Conjuntos_Siguiente.add(siguiente);
        return Conjuntos_Siguiente;
                
    }

    public static void Mostra() {
        for (int i = 0; i < Conjuntos_Siguiente.size(); i++) {
            System.out.println(Conjuntos_Siguiente.get(i));
        }
    }

    public static void Conjunto_S() {
        Collections.reverse(Conjuntos_Siguiente);
        
        for (int i = Conjuntos_Siguiente.size() - 1; i >= 0; i--) {
            String auxPr = Conjuntos_Siguiente.get(i).toString().substring(0, 5);
            for (int l = 0; l < Conjuntos_Siguiente.size(); l++) {
                String auxSe = Conjuntos_Siguiente.get(l).substring(0, 5);
                if (auxPr.equals(auxSe)) {
                    System.out.println(Conjuntos_Siguiente.get(l));
                    Conjuntos_Siguiente.remove(l);
                }
            }
        }
    }

    
    
    public static int getJ() {
        return j;
    }

    public static String[] getPosicion1() {
        return posicion_InicialIZQ;
    }

    public static String[] getPosicion2inicia() {
        return posicion_InicialDER;
    }

}
